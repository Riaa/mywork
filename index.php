<?php
get_header();
?>

    <!-- slide  stare -->
    <div class="slide">
        <div class="container">
            <?php
                $slider = new WP_Query(array(
                    'post_type'  =>  'post',
                    'category_name'  =>  'slider',
                    'posts_per_page'  =>  3,
                    'order'  =>  'ASC',
                ));
                $count=0;
            ?>
            <div class="bd-example">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                              <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                              <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            </ol>
                         
                                <div class="carousel-inner">
                                    <?php while($slider->have_posts()) : $slider->the_post();?>
                                     <div class="carousel-item <?php echo ($count == 0) ? 'active' : '' ?>">
                                        <?php the_post_thumbnail(); ?>
                                            <div class="carousel-caption d-none d-md-block">
                                              <h5><?php the_title(); ?></h5>
                                              <p><?php the_content(); ?></p>
                                            </div>
                                    </div>
                                    <?php 
                                         $count++;
                                        endwhile;
                                    ?>
                                </div>
                        
                 
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>
    </div>
    
    <!-- slide End -->

    <div class="content">
        <div class="container">
            <div class="row bg">
        

        <?php 
                    $servpost = new WP_Query(array(
                        'post_type'     =>'post',
                        'category_name' =>'service',
                        'order'         =>'DESC',
                        'post_per_page' => 4,
                    ));     
                ?>  

            <?php while($servpost->have_posts()) : $servpost->the_post(); ?>
                  <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate hsrv">
                    <div class="media block-6 d-block text-center">
                      <div class="icon d-flex justify-content-center align-items-center">
                            <?php the_post_thumbnail(); ?>
                      </div>
                      <div class="media-body p-2 mt-3">
                        <h3 class="heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p><?php read_more(13); ?><a href="<?php the_permalink(); ?>">...Read More</a></p>
                      </div>
                    </div>      
                  </div>
                <?php endwhile; ?>


               <!-- <div class="col-md-6">
                    <h2> Know About Us</h2>
                    <p class="text-justify">
                        Online has launched its connectivity services in the year 2011 having the commitment of quality internet service with an affordable price. At the time of commencement epnet started with broadband service with the state of art technology. In the course of time it’s team has developed and introduced many new technologies to meet the requirement of new millennium. In the last few years epnet has introduced many new ideas and technologies to the internet industry with our experienced team.
                    </p>
                </div>
                -->
            </div>
            </div>
        </div>



<?php
get_footer();
?>