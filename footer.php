
<div class="footer">
        <div class="container">
            <div class="copyright">

 <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
         <!-- <div class="col-md">
            <div class="ftco-footer-widget mb-5">
              <h2 class="ftco-heading-2 logo">
              	<img src="images/cbc-logo1.png" width="auto" height="auto" alt="Logo" />
              </h2>
              <p>The Chittagong Builders Corporation (CBC) is a famous and renowned Construction company in Bangladesh.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
           
          </div>
         -->
          <div class="col-md">
            <div class="ftco-footer-widget mb-5 ml-md-4">
              <h2 class="ftco-heading-2">Services</h2>

     <?php
        if(function_exists('wp_nav_menu')){
          wp_nav_menu(array(
            'theme_location'     => 'secondarymenu',
            'depth'              => '1', //1= no dropdown & 2= dropdown.
            'container'          => '',
            'container_class'    => '',
            'container_id'       => '',
            'menu_class'         => 'list-unstyled',
            'fallback_cb'        => 'wp_Bootstrap_Navwalker::fallback',
            'walker'             => new WP_Bootstrap_Navwalker(),
          ));
        }
    ?>

              <!--<ul class="list-unstyled">
                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Construction</a></li>
                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Renovation</a></li>
                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Painting</a></li>
                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Interior Design</a></li>
                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Exterior Design</a></li>
              </ul>-->
            </div>
          </div>

           <?php dynamic_sidebar('footer-widget'); ?>
         <!-- <div class="col-md-3">
            <div class="ftco-footer-widget mb-6">
              <h2 class="ftco-heading-2">Mirsorai Office</h2>
              <p>BEPZA Road Opposite Side<br/>Mirsarai Economic Zone,<br/>Chattogram.</p>
              <p>
              	<span class="icon-paper-plane"></span> info@cbc.com.bd<br/>
              </p>
            </div>
          </div>
         <div class="col-md-3">
            <div class="ftco-footer-widget mb-6">
              <h2 class="ftco-heading-2">Corporate Office</h2>
              <p>3705/B3/5898, North Soraipara<br/>Behind Hakkani-2 (Refueling Station),<br/>Pahartali, Chattogram</p>
              <p>
              	<span class="icon-phone2"></span> +88 01819-322295<br>
              	<span class="icon-phone2"></span> +88 01919-322295<br>
              	<span class="icon-phone2"></span> +88 01711-369282<br>
              	<span class="icon-phone2"></span> +88 01619-322295<br>
              </p>
            </div>
          </div>
         -->
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p>
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="http://olineit.com" target="_blank">Oline IT</a>
  </p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/popper.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/bootstrap.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.easing.1.3.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.waypoints.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.stellar.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/owl.carousel.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/aos.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.animateNumber.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/bootstrap-datepicker.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/jquery.timepicker.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/google-map.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri());?> /js/main.js"></script>
   

</div>
        </div>
    </div>

<script src="<?php echo esc_url(get_template_directory_uri());?>/js/jquery.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri());?>/bootstrap/js/bootstrap.bundle.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>