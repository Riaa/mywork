<?php
function mywork_function(){

	//adding navwalker file
	require_once get_template_directory(). '/wp-bootstrap-navwalker.php';
	//custom title
	add_theme_support('title-tag');

	//thumbnail
	add_theme_support('post-thumbnails');

	//custom background
	add_theme_support('custom-background');

	//custom logo
	add_theme_support('custom-logo',array(
		'defult-image' =>
			get_template_directory_uri().'/img/logo.png',

	));

	//load theme textdomain
	load_theme_textdomain('mywork',get_template_directory_uri().'languages');

	//register  menus
	if(function_exists('register_nav_menus')){
	register_nav_menus(array(
		'primarymenu'	=>__('Header Menu','mywork'),
		'secondarymenu'	=>__('Footer Menu','mywork')
	));
	}

//read more
	function read_more($limit){
		$post_content = explode(" ", get_the_content());

		$less_content = array_slice($post_content, 0, $limit);

		echo implode(" ", $less_content);
	}








	
}

add_action('after_setup_theme','mywork_function');


function mywork_widgets(){
	register_sidebar(array(
		'name'	=>	__('Footer Widgets','mywork'),
		'description'	=>	__('Add your footer widgets here','mywork'),
		'id'	=>	'footer-widget',
		'before_widget'	=>	'<div class="col-md-3"><div class="ftco-footer-widget mb-6">',
		'after_widget'	=>	'</p></div></div>',
		'before_title'	=>	'<h2 class="ftco-heading-2">',
		'after_title'	=>	'</h2><p>',
	));
}
add_action('widgets_init','mywork_widgets');
?>